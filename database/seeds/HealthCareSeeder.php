<?php

use Illuminate\Database\Seeder;

class HealthCareSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $section = new \App\Models\HealthCareSection();
        $section->create([
            'title' => 'سرعة الاستجابة',
            'content' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى.'
        ]);
        $section->create([
            'title' => 'طلب موعد',
            'content' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى.'
        ]);
        $section->create([
            'title' => 'جدول مواعيد الطبيب',
            'content' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى.'
        ]);
        $section->create([
            'title' => 'جدول مواعيد الطبيب',
            'content' => 'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى.'
        ]);
    }
}
