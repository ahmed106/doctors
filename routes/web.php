<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::namespace('Website')->group(function () {

    // home
    Route::get('/', 'FrontController@index')->name('home');
    // end home

    // about
    Route::get('about', 'AboutController@index');
    // end about

    // categories
    Route::get('categories', 'CategoryController@index');
    Route::get('categories/{id}', 'CategoryController@show');
    // end categories


    // blogs
    Route::get('blogs', 'BlogController@index');
    Route::get('blogs/{id}', 'BlogController@show');
    // end blogs

    //contact us
    Route::get('contact-us', 'ContactUsController@index');
    Route::post('contact-us', 'ContactUsController@post')->name('website.contactUs');
    // end contact us
});

