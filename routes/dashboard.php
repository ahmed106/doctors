<?php

use Illuminate\Support\Facades\Route;

Route::prefix('dashboard')->middleware('auth')->group(function () {
    Route::get('/', 'HomeController@index')->name('dashboard.index');

    // profile
    Route::get('/profile', 'HomeController@profile')->name('user.profile');
    Route::post('/profile', 'HomeController@editProfile')->name('user.editProfile');

    // end profile

    // sliders routes
    Route::get('sliders-data', 'SliderController@data')->name('sliders.data');
    Route::post('sliders-bulkDelete', 'SliderController@bulkDelete')->name('sliders.bulkDelete');
    Route::resource('sliders', 'SliderController')->except('show');
    //end sliders routes

    // about routes
    Route::get('about-data', 'AboutController@data')->name('about.data');
    Route::post('about-bulkDelete', 'AboutController@bulkDelete')->name('about.bulkDelete');
    Route::resource('about', 'AboutController')->except('show');
    //end about routes

    // customers routes
    Route::get('customers-data', 'CustomerController@data')->name('customers.data');
    Route::post('customers-bulkDelete', 'CustomerController@bulkDelete')->name('customers.bulkDelete');
    Route::resource('customers', 'CustomerController')->except('show');
    //end customers routes

    // blogs routes
    Route::get('blogs-data', 'BlogController@data')->name('blogs.data');
    Route::post('blogs-bulkDelete', 'BlogController@bulkDelete')->name('blogs.bulkDelete');
    Route::resource('blogs', 'BlogController')->except('show');
    //end customers routes

    // categories routes
    Route::get('categories-data', 'CategoryController@data')->name('categories.data');
    Route::post('categories-bulkDelete', 'CategoryController@bulkDelete')->name('categories.bulkDelete');
    Route::resource('categories', 'CategoryController')->except('show');
    //end categories routes

    // services routes
    Route::get('services-data', 'ServiceController@data')->name('services.data');
    Route::post('services-bulkDelete', 'ServiceController@bulkDelete')->name('services.bulkDelete');
    Route::resource('services', 'ServiceController')->except('show');
    //end services routes

    // Health Care Section routes
    Route::get('health-care-data', 'HealthCareSectionController@data')->name('health-care.data');
    Route::post('health-care-bulkDelete', 'HealthCareSectionController@bulkDelete')->name('health-care.bulkDelete');
    Route::resource('health-care', 'HealthCareSectionController')->except('show');
    //end  Health Care Section routes

    //pages
    Route::get('pages-data', 'PageController@data')->name('pages.data');
    Route::resource('pages', 'PageController')->except('show');
    //end pages

    // projects routes
    Route::get('contact-us-data', 'ContactUsController@data')->name('contact-us.data');
    Route::post('contact-us-bulkDelete', 'ContactUsController@bulkDelete')->name('contact-us.bulkDelete');
    Route::resource('contact-us', 'ContactUsController')->except('show');
    //end customers routes

    // settings
    Route::get('settings', 'SettingController@index')->name('settings.index');
    Route::post('settings', 'SettingController@store')->name('settings.store');
    // end settings


});

