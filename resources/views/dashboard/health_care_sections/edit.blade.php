@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">

                <form action="{{route('health-care.update',$HealthCareSection->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="card">
                        <div class="card-header">
                            <h3>تعديل السكشن</h3>
                        </div>
                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{$HealthCareSection->title}}" type="text" name="title" class="form-control">
                                        <span class="input-span">العنوان</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <textarea name="content" rows="3" class="form-control editor">{{$HealthCareSection->content}}</textarea>
                                        <span class="input-span">المحتوي</span>
                                    </div>
                                </div>
                            </div>
                            {{--                            <div class="row">--}}
                            {{--                                <div class="col-md-12">--}}
                            {{--                                    <div class="input-custom">--}}
                            {{--                                        <label class="">الصورة ( مقاس الصورة -- 670 طول * 770 عرض )</label>--}}
                            {{--                                        <input id="photo" type="file" name="photo">--}}
                            {{--                                        <img width="100" id="preview" height="100" src="{{$HealthCareSection->image}}" alt="">--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}
                            {{--                            </div>--}}


                        </div>
                        <div class="card-footer">
                            <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                        </div>
                    </div>
                </form>

            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->
@endsection
