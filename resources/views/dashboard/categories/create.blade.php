@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">

                <form action="{{route('categories.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h3>إضافه قسم</h3>
                        </div>
                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{old('title')}}" type="text" name="title" class="form-control">
                                        <span class="input-span">العنوان</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <textarea name="content" rows="3" class="form-control editor">{{old('content')}}</textarea>
                                        <span class="input-span">المحتوي</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <label class="">الصورة ( مقاس الصورة -- 670 طول * 770 عرض )</label>
                                        <input id="photo" type="file" name="photo">
                                        <img width="100" id="preview" height="100" src="{{asset('default.svg')}}" alt="">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <label class="">الأيقون</label>
                                        <select name="icon" id="" class="form-control">
                                            <option value="flaticon-hematology">flaticon hematology</option>
                                            <option value="flaticon-neurology">flaticon neurology</option>
                                            <option value="flaticon-gastroenterology">flaticon gastroenterology</option>
                                            <option value="flaticon-pulmonology">flaticon pulmonology</option>
                                            <option value="flaticon-cardiology">flaticon cardiology</option>
                                            <option value="flaticon-ophthalmology">flaticon ophthalmology</option>
                                        </select>
                                        {{--                                       <i class=" departments-item-icon flaticon-ophthalmology"></i>--}}
                                    </div>
                                </div>
                            </div>


                            <div class="accordion-container">
                                <div class="set">
                                    <a href="#">
                                        بيانات السيو SEO
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="box-custom">
                                        <div class="flex-divs">
                                            <div class="input-custom">
                                                <input value="" type="text" name="meta_title" class="form-control">
                                                <span class="input-span">عنوان الميتا</span>
                                            </div>
                                            <div class="input-custom">
                                                <input value="" type="text" name="meta_keywords" class="form-control">
                                                <span class="input-span">الكلمات الدلالية</span>
                                            </div>
                                            <div class="input-custom">
                                                <textarea name="meta_description" class="form-control editor"></textarea>
                                                <span class="input-span">وصف الميتا</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                        </div>
                    </div>
                </form>

            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->
@endsection
