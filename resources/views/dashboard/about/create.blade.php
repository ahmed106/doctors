@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">

                <form action="{{route('about.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h3>من نحن</h3>
                        </div>
                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input name="title" value="{{$about?$about->title:old('title')}}" type="text" class="form-control">
                                        <span class="input-span">العنوان</span>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <textarea name="content" rows="3" class="form-control editor">{{$about?$about->content:old('content')}}</textarea>
                                        <span class="input-span">التفاصيل</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <textarea name="missions" rows="3" class="form-control editor">{{$about?$about->missions:old('missions')}}</textarea>
                                        <span class="input-span">القائمه الجانبيه</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <textarea name="vision" rows="3" class="form-control editor">{{$about?$about->vision:old('vision')}}</textarea>
                                        <span class="input-span">الأقسام</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <textarea name="plans" rows="3" class="form-control editor">{{$about?$about->plans:old('plans')}}</textarea>
                                        <span class="input-span">المقالات</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <input name="experience_number" value="{{$about?$about->experience_number:old('experience_number')}}" type="number" class="form-control">
                                        <span class="input-span">عدد سنوات الخبره </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <input type="number" name="doctor_experienced_number" value="{{$about?$about->doctor_experienced_number:old('doctor_experienced_number')}}" class="form-control">
                                        <span class="input-span">عدد الأطباء الخبره</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <input type="number" name="healthy_patient" value="{{$about?$about->healthy_patient:old('healthy_patient')}}" class="form-control">
                                        <span class="input-span">عدد المرضي المعافين</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <input name="health_departments" value="{{$about?$about->health_departments:old('health_departments')}}" type="number" class="form-control">
                                        <span class="input-span">عدد الإدارات الصحية</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <input name="branches" value="{{$about?$about->branches:old('branches')}}" type="number" class="form-control">
                                        <span class="input-span">عدد الفروع</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <label class="">الصورة ( مقاس الصورة -- 412 طول * 512 عرض )</label>
                                        <input id="photo" type="file" name="photo">
                                        @if($about && $about->photo)
                                            <img width="100" id="preview" height="100" src="{{$about->image}}" alt="">
                                        @else
                                            <img width="100" id="preview" height="100" src="{{asset('default.svg')}}" alt="">
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class="accordion-container">
                                <div class="set">
                                    <a href="#">
                                        بيانات السيو SEO
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="box-custom">
                                        <div class="flex-divs">
                                            <div class="input-custom">
                                                <input value="{{$about?$about->meta_title:old('meta_title')}}" type="text" name="meta_title" class="form-control">
                                                <span class="input-span">عنوان الميتا</span>
                                            </div>
                                            <div class="input-custom">
                                                <input value="{{$about?$about->meta_keywords:old('meta_keywords')}}" type="text" name="meta_keywords" class="form-control">
                                                <span class="input-span">الكلمات الدلالية</span>
                                            </div>
                                            <div class="input-custom">
                                                <textarea name="meta_description" class="form-control editor">{{$about?$about->meta_description:old('meta_description')}}</textarea>
                                                <span class="input-span">وصف الميتا</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                        </div>
                    </div>
                </form>

            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->
@endsection
