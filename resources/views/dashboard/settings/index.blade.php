@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">

                <form action="{{route('settings.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h3>إعدادات الموقع</h3>
                        </div>
                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{isset($setting) ? $setting->website_name :''}}" type="text" name="website_name" class="form-control">
                                        <span class="input-span">اسم الموقع</span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{isset($setting) ? $setting->website_address :''}}" type="text" name="website_address" class="form-control">
                                        <span class="input-span">العنوان</span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{isset($setting) ? $setting->website_email :''}}" type="text" name="website_email" class="form-control">
                                        <span class="input-span">البريد الالكتروني</span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{isset($setting) ? $setting->website_phone :''}}" type="text" name="website_phone" class="form-control">
                                        <span class="input-span">الهاتف</span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{isset($setting) ? $setting->website_facebook :''}}" type="text" name="website_facebook" class="form-control">
                                        <span class="input-span">facebook</span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{isset($setting) ? $setting->website_twitter :''}}" type="text" name="website_twitter" class="form-control">
                                        <span class="input-span">twitter</span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{isset($setting) ? $setting->website_linked_in :''}}" type="text" name="website_linked_in" class="form-control">
                                        <span class="input-span">linked in </span>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <textarea name="map" id="" cols="178" rows="10">
                                            {{$setting?$setting->map:''}}
                                        </textarea>
                                        <span class="input-span">Map iframe </span>
                                    </div>
                                </div>


                                <div id="render_mape" class="col-md-12">
                                    {!! $setting?$setting->map:'' !!}
                                </div>

                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <label class="">الصورة (logo)</label>
                                        <input id="photo" type="file" name="logo">
                                        @if(isset($setting->logo))
                                            <img width="100" id="preview" height="100" src="{{asset('images/settings/'.$setting->logo->src)}}" alt="">
                                        @else
                                            <img width="100" id="preview" height="100" src="{{asset('default.svg')}}" alt="">
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="accordion-container">
                                <div class="set">
                                    <a href="#">
                                        بيانات السيو SEO
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="box-custom">
                                        <div class="flex-divs">
                                            <div class="input-custom">
                                                <input value="{{$setting?$setting->meta_title:''}}" type="text" name="meta_title" class="form-control">
                                                <span class="input-span">عنوان الميتا</span>
                                            </div>

                                            <div class="input-custom">
                                                <textarea name="meta_description" class="form-control editor">{{$setting?$setting->meta_description:''}}</textarea>
                                                <span class="input-span">وصف الميتا</span>
                                            </div>
                                            <div class="input-custom">
                                                <input value="{{$setting?$setting->meta_keywords:''}}" type="text" name="meta_keywords" class="form-control">
                                                <span class="input-span">الكلمات الدلالية</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-container">
                                <div class="set">
                                    <a href="javascript:void();">
                                        بيانات الهيدر
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="box-custom">
                                        <div class="flex-divs">

                                            <div class="input-custom">
                                                <textarea name="header" class="form-control ">{{$setting?$setting->header:''}}</textarea>
                                                <span class="input-span"> بيانات الهيدر</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="accordion-container">
                                <div class="set">
                                    <a href="javascript:void();">
                                        بيانات الفوتر
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="box-custom">
                                        <div class="flex-divs">

                                            <div class="input-custom">
                                                <textarea name="footer" class="form-control ">{{$setting?$setting->footer:''}}</textarea>
                                                <span class="input-span">بيانات الفوتر</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                        </div>
                    </div>
                </form>

            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->
@endsection
@push('js')
    <script>
        $('textarea[name="map"]').on('keyup', function () {
            $('#render_mape').html($(this).val());
        });
    </script>
@endpush
