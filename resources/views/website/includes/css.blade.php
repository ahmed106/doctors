<link rel="stylesheet" href="{{asset('assets/website')}}/css/bootstrap.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Public+Sans:wght@400;500;600;700&amp;family=Rajdhani:wght@400;500;600;700&amp;display=swap">
<link rel="stylesheet" href="{{asset('assets/website')}}/fonts/fontawesome/css/all.min.css">
<link rel="stylesheet" href="{{asset('assets/website')}}/fonts/flaticon/flaticon.css">
<link rel="stylesheet" href="{{asset('assets/website')}}/css/animate.css">
<link rel="stylesheet" href="{{asset('assets/website')}}/css/owl.carousel.min.css">
<link rel="stylesheet" href="{{asset('assets/website')}}/css/owl.theme.default.min.css">
<link rel="stylesheet" href="{{asset('assets/website')}}/css/lity.min.css">
<link rel="stylesheet" href="{{asset('assets/website')}}/css/magnific-popup.css">
<!-- <link rel="stylesheet" href="{{asset('assets/website')}}/css/style.css"> -->
<link rel="stylesheet" href="{{asset('assets/website')}}/css/style-rtl.css">

<link rel="stylesheet" href="{{asset('assets/dashboard/plugins/toastr/toastr.css')}}">
@toastr_css
