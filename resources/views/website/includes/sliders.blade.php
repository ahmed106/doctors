<section class="header">
    <div class="header-carousel owl-carousel owl-theme">
        @foreach($sliders as $slider)
            <div class="sec-hero display-table" style="background-image: url({{$slider->image}})">
                <div class="table-cell">
                    <div class="overlay"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="banner">
                                    <h1 class="handline">{{$slider->title}}</h1>
                                    <p class="about-website">
                                        {!! $slider->content !!}</p>
                                    <a class="btn-1 btn-2" href="{{url('about')}}">قراءة المزيد</a>
                                    <a class="btn-1 btn-3 ml-30" href="{{url('contact-us')}}">تواصل معنا</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach


    </div>
</section>
