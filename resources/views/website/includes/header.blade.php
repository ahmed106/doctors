<!-- :: All Navbar -->
<header class="all-navbar" id="page">

    <!-- :: Top Navbar -->
    <div class="top-navbar">
        <div class="container">
            <div class="content-box d-flex align-items-center justify-content-between">
                <ul class="website-info">
                    <li>
                        <i class="far fa-envelope"></i>
                        <a href="mailto:{{$setting->website_email}}">{{$setting->website_email}}</a>
                    </li>
                    <li>
                        <i class="fas fa-map-marker-alt"></i>
                        {{$setting->website_address}}
                    </li>
                </ul>
                <ul class="website-icon-social">
                    <li><a href="{{$setting->website_facebook}}"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="{{$setting->website_twitter}}"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="{{$setting->website_linked_in}}"><i class="fab fa-linkedin-in"></i></a></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- :: Navbar -->
    <nav class="nav-bar">
        <div class="container">
            <div class="content-box d-flex align-items-center justify-content-between">
                <div class="logo">
                    <a href="{{url('/')}}" class="logo-nav">
                        <img class="img-fluid one" src="{{$setting->image}}" alt="01 Logo">
                    </a>
                    <a href="#open-nav-bar-menu" class="open-nav-bar">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
                <div class="nav-bar-links" id="open-nav-bar-menu">
                    <ul class="level-1">
                        @foreach($pages as $page)
                            <li class="item-level-1">
                                <a href="{{url($page->url)}}" class="link-level-1">{{$page->name}}</a>
                            </li>
                        @endforeach


                    </ul>
                </div>
                <ul class="nav-bar-tools d-flex align-items-center justify-content-between">
                    <li class="item phone">
                        <div class="nav-bar-contact">
                            <i class="flaticon-emergency-call"></i>
                            <div class="content-box">
                                <span>رقم الطوارئ</span>
                                <a href="tel:{{$setting->website_phone}}">{{$setting->website_phone}}</a>
                            </div>
                        </div>
                    </li>
                    <li class="item">
                        <span class="menu-icon open"><i class="fas fa-list"></i></span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

</header>
