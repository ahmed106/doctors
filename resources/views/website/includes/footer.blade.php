<!-- :: Footer -->
<footer class="footer">
    <div class="footer-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="logo">
                        <img class="img-fluid" src="{{asset('assets/website')}}/images/logo/02_logo.png" alt="Footer Logo">
                    </div>
                    <ul class="links">
                        @foreach($pages as $page)
                            <li><a href="{{url($page->url)}}">{{$page->name}}</a></li>
                        @endforeach


                    </ul>
                    <ul class="footer-icon">
                        <li><a href="{{$setting->website_facebook}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="{{$setting->website_twitter}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="{{$setting->website_linked_in}}" target="_blank"><i class="fab fa-linkedin"></i></a></li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <p>2021 © جميع الحقوق محفوظة لدي شركة <a href="https://codlop.sa/ar">Codlop</a></p>
        </div>
    </div>
</footer>
