<!-- :: jQuery JS -->
<script src="{{asset('assets/website')}}/js/jquery-3.6.0.min.js"></script>
<script src="{{asset('assets/website')}}/js/bootstrap.bundle.min.js"></script>
<script src="{{asset('assets/website')}}/js/owl.carousel.min.js"></script>
<script src="{{asset('assets/website')}}/js/lity.min.js"></script>
<script src="{{asset('assets/website')}}/js/jquery.magnific-popup.min.js"></script>
<script src="{{asset('assets/website')}}/js/jquery.waypoints.min.js"></script>
<script src="{{asset('assets/website')}}/js/jquery.counterup.min.js"></script>
<script src="{{asset('assets/website')}}/js/main.js?v=1"></script>

<script src="{{asset('assets/dashboard/plugins/toastr/toastr.js')}}"></script>
<script src="{{asset('assets/dashboard/plugins/sweet_alert/swal.js')}}"></script>
@include('dashboard.includes.swal')
@toastr_render

