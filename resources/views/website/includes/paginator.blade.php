@if ($paginator->hasPages())
    <div class="row">
        <div class="col">
            <div class="pagination-area">


                <ul class="pagination">

                    @if ($paginator->onFirstPage())
                        <li class="disabled"> -> السابق</li>

                    @else
                        <li class="active"><a href="{{ $paginator->previousPageUrl() }}" rel="prev">← السابق</a></li>
                    @endif



                    @foreach ($elements as $element)

                        @if (is_string($element))
                            <li class="disabled">{{ $element }}</li>
                        @endif



                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <li class="active">{{ $page }}</li>
                                @else
                                    <li><a href="{{ $url }}">{{ $page }}</a></li>
                                @endif
                            @endforeach
                        @endif
                    @endforeach



                    @if ($paginator->hasMorePages())
                        <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">التالي →</a></li>
                    @else
                        <li class="disabled">التالي →</li>
                    @endif
                </ul>


                {{--            @if($paginator->hasPages())--}}
                {{--                <ul class="pagination">--}}
                {{--                    @if($paginator->onFirstPage())--}}
                {{--                        <li class="disabled">السابق</li>--}}
                {{--                    @else--}}
                {{--                        <li class=""><a href="{{$paginator->previousPageUrl()}}" rel="prev"></a>السابق</li>--}}
                {{--                    @endif--}}

                {{--                    @foreach ($elements as $element)--}}


                {{--                        @if (is_string($element))--}}
                {{--                            <li class="disabled"><span>{{ $element }}</span></li>--}}
                {{--                        @endif--}}



                {{--                        @if (is_array($element))--}}
                {{--                            @foreach ($element as $page => $url)--}}
                {{--                                @if ($page == $paginator->currentPage())--}}
                {{--                                    <li class="active">{{ $page }}</li>--}}
                {{--                                @else--}}
                {{--                                    <li><a href="{{ $url }}">{{ $page }}</a></li>--}}
                {{--                                @endif--}}
                {{--                            @endforeach--}}
                {{--                        @endif--}}
                {{--                    @endforeach--}}

                {{--                    @if ($paginator->hasMorePages())--}}
                {{--                    <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">التالي →</a></li>--}}
                {{--                    @else--}}
                {{--                        <li class="disabled"><span>التالي →</span></li>--}}
                {{--                    @endif--}}

                {{--                </ul>--}}
                {{--            @endif--}}


            </div>
        </div>
    </div>


