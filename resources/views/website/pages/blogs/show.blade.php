@extends('website.layouts.master')
@push('title')
    {{$page_title}} |  {{$blog->title}}
@endpush
@section('content')

    <!-- :: Breadcrumb Header -->
    <section class="breadcrumb-header" style="background-image: url({{asset('assets/website')}}/images/header/06_header.jpg)">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="banner">
                        <h1>{{$page_title}}</h1>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسية</a></li>
                            <li><i class="fas fa-angle-right"></i></li>
                            <li>{{$page_title}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="single-bolg py-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="blog-item">
                        <div class="img-box">
                            <img class="img-fluid" src="{{$blog->image}}" alt="03 Blog">
                        </div>
                        <div class="text-box">
                            <span class="blog-date">{{\Carbon\Carbon::create($blog->created_at)->toDateString()}}</span>
                            <h5>{{$blog->title}}</h5>
                            <p>{!! $blog->content !!}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="sidebar-blog ml-20">
                        <div class="widget">
                            <div class="widget-title">
                                <h3>تابعنا على</h3>
                            </div>
                            <div class="widget-body">
                                <div class="follow">
                                    <ul class="icon">
                                        <li><a href="{{$setting->website_facebook}}"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="{{$setting->website_twitter}}"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="{{$setting->website_linked_in}}"><i class="fab fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="widget">
                            <div class="widget-title">
                                <h3>اخبار ونصائح</h3>
                            </div>
                            <div class="news-box">
                                @foreach($blogs_ as $blog)
                                    <div class="news-item">
                                        <a href="{{url('blogs/'.$blog->id)}}">
                                            <img class="img-fluid" src="{{$blog->image}}" alt="01 Blog">
                                        </a>
                                        <div class="item-content">
                                            <span><a>{{\Carbon\Carbon::create($blog->created_at)->toDateString()}}</a></span>
                                            <a class="title-blog" href="{{url('blogs/'.$blog->id)}}">
                                                <h5>{{$blog->title}}</h5>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
