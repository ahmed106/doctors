@extends('website.layouts.master')

@push('title')
    {{$page_title}}
@endpush
@section('content')
    <!-- :: Breadcrumb Header -->
    <section class="breadcrumb-header" style="background-image: url({{asset('assets/website')}}/images/header/06_header.jpg)">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="banner">
                        <h1>المقالات</h1>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسية</a></li>
                            <li><i class="fas fa-angle-right"></i></li>
                            <li>{{$page_title}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- :: Blog -->
    <section class="blog py-100">
        <div class="container">
            <div class="sec-title">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>{{$page_title}}</h2>
                        <h3>بعض المقالات التي قد تساعدك</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($blogs_ as $blog)
                    <div class="col-md-6 col-lg-4">
                        <div class="blog-item">
                            <div class="img-box">
                                <a href="{{url('blogs/'.$blog->id)}}" class="open-post">
                                    <img class="img-fluid" src="{{$blog->image}}" alt="01 Blog">
                                </a>
                            </div>
                            <div class="text-box">
                                <span class="blog-date">{{\Carbon\Carbon::create($blog->created_at)->toDateString()}}</span>
                                <a href="{{url('blogs/'.$blog->id)}}" class="title-blog">
                                    <h5>{{$blog->title}}</h5>
                                </a>
                                <div class="desc">{!! $blog->content !!}</div>
                                <a href="{{url('blogs/'.$blog->id)}}" class="link">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>


            @if($blogs_->links()->paginator->hasPages())
                <div class="row">
                    <div class="col">
                        <div class="pagination-area">
                            <ul class="pagination">
                                @if($blogs_->links()->paginator->currentPage() == $blogs_->links()->paginator->onFirstPage() )
                                    <li class="disabled"><- السابق</li>

                                @else
                                    <li class="active"><a href="{{$blogs_->links()->paginator->previousPageUrl()}}"> <- السابق</a></li>
                                @endif

                                @if($blogs_->links()->paginator->hasMorePages())

                                    <li class="active"><a href="{{$blogs_->links()->paginator->nextPageUrl()}}"> التالي -></a></li>
                                @else
                                    <li class="disabled">التالي -></li>
                                @endif


                            </ul>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </section>
@endsection
