@extends('website.layouts.master')
@push('title')
    {{$page_title}}
@endpush

@section('content')

    <!-- :: Breadcrumb Header -->
    <section class="breadcrumb-header" style="background-image: url({{asset('assets/website')}}/images/header/06_header.jpg)">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="banner">
                        <h1>{{$page_title}}</h1>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسية</a></li>
                            <li><i class="fas fa-angle-right"></i></li>
                            <li>{{$page_title}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- :: About Us -->
    <section class="about-us about-us-2 py-100" id="start">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="about-text-box">
                        <div class="sec-title sec-title-2">
                            <h2>{{$page_title}}</h2>
                            <h3>{{$about?$about->title:''}}</h3>
                            <p class="sec-explain">
                                {!! $about?$about->content:'' !!}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="about-img-box">
                        <div class="img-box">
                            <img class="img-fluid" src="{{$about?$about->image:''}}" alt="02 About">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- :: Statistic -->
    <div class="statistic">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <div class="statistic-item">
                        <i class="flaticon-doctor"></i>
                        <div class="content">
                            <div class="statistic-counter">{{$about?$about->experience_number:''}}</div>
                            <div class="counter-name">أطباء خبراء</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="statistic-item">
                        <i class="flaticon-rhinology"></i>
                        <div class="content">
                            <div class="statistic-counter">{{$about?$about->healthy_patient:''}}</div>
                            <div class="counter-name">مريض معافي</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="statistic-item">
                        <i class="flaticon-medicine"></i>
                        <div class="content">
                            <div class="statistic-counter">{{$about?$about->health_departments:''}}</div>
                            <div class="counter-name">الإدارات الصحية</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="statistic-item">
                        <i class="flaticon-stethoscope"></i>
                        <div class="content">
                            <div class="statistic-counter">{{$about?$about->branches:''}}</div>
                            <div class="counter-name">الفروع</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
