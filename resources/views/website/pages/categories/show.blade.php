@extends('website.layouts.master')
@push('title')
    {{$page_title}} |     {{$category->title}}
@endpush
@section('content')
    <!-- :: Breadcrumb Header -->
    <section class="breadcrumb-header" style="background-image: url({{asset('assets/website')}}/images/header/06_header.jpg)">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="banner">
                        <h1>{{$page_title}}</h1>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسية</a></li>
                            <li><i class="fas fa-angle-right"></i></li>
                            <li>{{$page_title}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- :: Department Details -->
    <section class="single-department py-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="sidebar-department mr-20">
                        <div class="widget">
                            <div class="widget-title">
                                <h3>{{$page_title}}</h3>
                            </div>
                            <div class="widget-body">
                                <ul class="single-department-list">
                                    @foreach($categories as $cat)
                                        <li class="{{$cat->id==$category->id?'active':''}}"><a href="{{url('categories/'.$cat->id)}}">{{$cat->title}}<i class="fas fa-angle-right"></i></a></li>
                                    @endforeach


                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <img class="img-fluid mb-4" src="{{$category->image}}" alt="">
                    <div class="single-department-box">
                        <h2>{{$category->title}}</h2>
                        <p>{!! $category->content !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
