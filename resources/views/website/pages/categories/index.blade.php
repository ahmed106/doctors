@extends('website.layouts.master')
@push('title')
    {{$page_title}}
@endpush
@section('content')
    <!-- :: Breadcrumb Header -->
    <section class="breadcrumb-header" style="background-image: url({{asset('assets/website')}}/images/header/06_header.jpg)">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="banner">
                        <h1>{{$page_title}}</h1>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسية</a></li>
                            <li><i class="fas fa-angle-right"></i></li>
                            <li>{{$page_title}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- :: departments -->
    <section class="departments departments-page py-100">
        <div class="container">
            <div class="sec-title">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>أفضل رعاية ممارسة</h2>
                        <h3>{{$page_title}}</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($categories as $category)
                    <div class="col-md-4">
                        <a href="{{url('categories/'.$category->id)}}" class="departments-item">
                            <div class="departments-item-img-box">
                                <img src="{{$category->image}}" alt="">
                                <span></span>
                            </div>
                            <div class="departments-item-text-box">
                                @if(!$category->icon)

                                    <i class="departments-item-icon flaticon-neurology "></i>
                                @else

                                    <i class="departments-item-icon  {{$category->icon}} "></i>
                                @endif
                                <div class="item-content">
                                    <h4>{{$category->title}}</h4>
                                    <div class="desc">{!! \Illuminate\Support\Str::limit($category->content,300) !!}</div>
                                    <em class="more">قراءة المزيد</em>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach


            </div>
        </div>
    </section>
@endsection
