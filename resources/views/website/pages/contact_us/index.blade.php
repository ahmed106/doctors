@extends('website.layouts.master')
@push('title')
    {{$page_title}}
@endpush
@section('content')
    <!-- :: Breadcrumb Header -->
    <section class="breadcrumb-header" style="background-image: url({{asset('assets/website')}}/images/header/06_header.jpg)">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="banner">
                        <h1> {{$page_title}}</h1>
                        <ul>
                            <li><a href="{{url('/')}}">الرئيسية</a></li>
                            <li><i class="fas fa-angle-right"></i></li>
                            <li> {{$page_title}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- :: تواصل معنا Page -->
    <div class="contact-us py-100">
        <div class="container">
            <div class="contact-info-content">
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="contact-box">
                            <i class="flaticon-call"></i>
                            <div class="box">
                                <a class="phone" href="tel:{{$setting->website_phone}}">{{$setting->website_phone}}</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="contact-box">
                            <i class="flaticon-email"></i>
                            <div class="box">
                                <a class="mail" href="mailto:{{$setting->Website_email}}">{{$setting->website_email}}</a>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="contact-box">
                            <i class="flaticon-location"></i>
                            <div class="box">
                                <p>{{$setting->website_address}}</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <!-- :: Map -->
                    <div class="map-box">
                        {!! $setting->map !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <form action="{{route('website.contactUs')}}" method="post">
                        @csrf
                        <div class="add-comments">
                            <div class="inner-add-comments">
                                <div class="row">
                                    <div class="col-md-6 inner-add-comments-box">
                                        <input type="text" name="name" placeholder="الاسم">
                                    </div>
                                    <div class="col-md-6 inner-add-comments-box">
                                        <input type="number" name="phone" placeholder="رقم الهاتف">
                                    </div>
                                    <div class="col-md-12 inner-add-comments-box">
                                        <input type="email" name="email" placeholder="البريد الالكتروني">
                                    </div>
                                    <div class="col-md-12 inner-add-comments-box">
                                        <textarea name="body" placeholder="اكتب رسالتك"></textarea>
                                    </div>
                                    <div class="col-md-12 inner-add-comments-box last">
                                        <button class="btn-1" type="submit">ارسال</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection


