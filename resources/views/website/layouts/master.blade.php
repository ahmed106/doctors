<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="{{asset('assets/website')}}/images/favicon.png">
    {!! $setting?$setting->header:'' !!}
    <title>{{$setting?$setting->website_name:''}} | @stack('title')</title>
    @include('website.includes.css')
</head>

<body>
<!-- :: Loading -->
<div class="loading">
    <div class="loading-box">
        <img class="img-fluid" src="{{asset('assets/website')}}/images/loading.gif" alt="Loading">
    </div>
</div>

@include('website.includes.header')
<div class="menu-box">
    <div class="inner-menu">
        <div class="website-info">
            <a href="" class="logo"><img class="img-fluid" src="{{$setting->image}}" alt="01 Logo"></a>
            <p>
                {!! strip_tags($about?$about->missions:'') !!}
            </p>
        </div>
        <div class="contact-info">
            <h4>بيانات التواصل</h4>
            <div class="contact-box">
                <i class="flaticon-call"></i>
                <div class="box">
                    <a class="phone" href="tel:{{$setting->website_phone}}">{{$setting->website_phone}}</a>

                </div>
            </div>
            <div class="contact-box">
                <i class="flaticon-email"></i>
                <div class="box">
                    <a class="mail" href="mailto:{{$setting->website_email}}">{{$setting->website_email}}</a>

                </div>
            </div>
            <div class="contact-box">
                <i class="flaticon-location"></i>
                <div class="box">
                    {{$setting->website_address}}
                </div>
            </div>
        </div>
        <div class="follow-us">
            <h4>تابعنا على</h4>
            <ul class="icon-follow">
                <li><a href="{{$setting->website_facebook}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="{{$setting->website_twitter}}" target="_blank"><i class="fab fa-twitter"></i></a></li>

                <li><a href="{{$setting->website_linked_in}}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
            </ul>
        </div>
        <span class="menu-box-icon exit"><i class="fas fa-times"></i></span>
    </div>
</div>
@yield('content')

@include('website.includes.footer')


<!-- :: Scroll Up -->
<div class="scroll-up">
    <a class="move-section" href="#page">
        <i class="fas fa-long-arrow-alt-up"></i>
    </a>
</div>

@include('website.includes.js')
{!! $setting?$setting->footer:'' !!}
</body>
</html>
