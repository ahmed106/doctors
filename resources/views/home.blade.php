@extends('website.layouts.master')
<style>
    .sec-title p {
        color: #1a3d7d;
    }
</style>
@section('content')



    <!-- :: Header -->
    @include('website.includes.sliders')

    <!-- :: Features -->
    <section class="features">
        <div class="container">
            <div class="row all-features-item">
                @php
                    $sort = [
                            0=>'one',
                            1=>'two',
                            2=>'three',
                        ];
                    $icons =[
                                0=>'flaticon-air-ambulance',
                                1=>'flaticon-medical-folder',
                                2=>'flaticon-medical-file',

                            ];
                @endphp

                @foreach($services as $index => $service)

                    <div class="col-lg-4 padding-0">
                        <div class="features-item {{$sort[$index]}}">
                            <i class="features-icon {{$icons[$index]}}"></i>
                            <div class="item-text">
                                <h4>{{$service->title}}</h4>
                                <p>{!! strip_tags($service->content) !!}</p>
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>
        </div>
    </section>

    <!-- :: About Us -->
    <section class="about-us py-100" id="start">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <div class="about-img-box">
                        <div class="img-box">
                            <img class="img-fluid" src="{{$about?$about->image:''}}" alt="01 About">
                        </div>
                        <div class="about-experience">
                            <i class="flaticon-experience"></i>
                            <div class="experience-counter">{{$about?$about->experience_number:''}}</div>
                            <h5>عام من<br>الخبرة</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="about-text-box">
                        <div class="sec-title">
                            <h2>{{$about?$about->title:''}}</h2>
                            <h3>الحصول على أفضل & تجربة مذهلة مع أطبائنا المحترفين</h3>
                            <p class="sec-explain">
                                {!! $about?$about->content:''!!}
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- :: Departments -->
    <section class="departments">
        <div class="bg-section">
            <div class="overlay overlay-2"></div>
        </div>
        <div class="container">
            <div class="sec-title">
                <div class="row">
                    <div class="col-lg-5">
                        <h2>أفضل رعاية ممارسة</h2>
                        <h3>أنواع مختلفة من الأقسام</h3>
                    </div>
                    <div class="col-lg-5">
                        <p class="sec-explain">{!! strip_tags($about?$about->vision:'')  !!}</p>
                        <a class="btn-1 btn-3 sec-btn" href="{{url('categories')}}">عرض كل الاقسام</a>
                    </div>
                </div>
            </div>
            <div class="departments-carousel departments-home owl-carousel owl-theme">
                @foreach($categories as $category)
                    <a href="{{url('categories/'.$category->id)}}" class="departments-item">
                        <div class="departments-item-text-box">
                            @if(!$category->icon)

                                <i class="departments-item-icon flaticon-neurology "></i>
                            @else

                                <i class="departments-item-icon  {{$category->icon}} "></i>
                            @endif
                            <div class="item-content">
                                <h4>{{$category->title}}</h4>
                                <div class="desc">{!! strip_tags(\Illuminate\Support\Str::title($category->content,300)) !!}</div>
                                <em class="more">قراءة المزيد</em>
                            </div>
                        </div>
                        <div class="departments-item-img-box">
                            <img src="{{$category->image}}" alt="">
                            <span></span>
                        </div>
                    </a>
                @endforeach


            </div>
        </div>
    </section>


    <!-- :: Provide -->
    <section class="provide">
        <div class="bg-section">
            <div class="overlay overlay-3"></div>
        </div>
        <div class="container">
            <div class="sec-title">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>رعاية طبية عالية الجودة</h2>
                        <h3>حلول رعاية صحية كاملة للجميع</h3>
                    </div>
                </div>
            </div>
            @php
                $icons = [
                                0=>'flaticon-doctor-1',
                                1=>'flaticon-experience',
                                2=>'flaticon-cross',
                                3=>'flaticon-medicine',
                         ];
            @endphp
            <div class="provide-content">
                <div class="row">
                    @foreach($health_care->take(4) as $index => $health)
                        <div class="col-md-6 col-lg-3">
                            <div class="provide-content-box">
                                <i class="{{$icons[$index]}}"></i>
                                <div class="content-box">
                                    <h4>{{$health->title}}</h4>
                                    <p>{!! $health->content !!}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach


                </div>
            </div>

        </div>
    </section>

    <!-- :: Statistic -->
    <div class="statistic">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <div class="statistic-item">
                        <i class="flaticon-doctor"></i>
                        <div class="content">
                            <div class="statistic-counter">{{$about?$about->doctor_experienced_number:''}}</div>
                            <div class="counter-name">أطباء خبراء</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="statistic-item">
                        <i class="flaticon-rhinology"></i>
                        <div class="content">
                            <div class="statistic-counter">{{$about?$about->healthy_patient:''}}</div>
                            <div class="counter-name">مريض معافي</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="statistic-item">
                        <i class="flaticon-medicine"></i>
                        <div class="content">
                            <div class="statistic-counter">{{$about?$about->health_departments:''}}</div>
                            <div class="counter-name">الإدارات الصحية</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="statistic-item">
                        <i class="flaticon-stethoscope"></i>
                        <div class="content">
                            <div class="statistic-counter">{{$about?$about->branches:''}}</div>
                            <div class="counter-name">الفروع</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- :: Blog -->
    <section class="blog">
        <div class="container">
            <div class="sec-title">
                <div class="row">
                    <div class="col-lg-5">
                        <h2>آخر المقالات</h2>
                        <h3>بعض المقالات التي قد تساعدك</h3>
                    </div>
                    <div class="col-lg-5">
                        <p class="sec-explain">{!! $about?$about->plans:'' !!}</p>
                        <a class="btn-1 sec-btn" href="{{url('blogs')}}">عرض كل المقالات</a>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($blogs->take(3) as $blog)
                    <div class="col-md-6 col-lg-4">
                        <div class="blog-item">
                            <div class="img-box">
                                <a href="{{url('blogs/'.$blog->id)}}" class="open-post">
                                    <img class="img-fluid" src="{{$blog->image}}" alt="01 Blog">
                                </a>
                            </div>
                            <div class="text-box">
                                <span class="blog-date">{{\Carbon\Carbon::create($blog->created_at)->toDateString()}}</span>
                                <a href="{{url('blogs/'.$blog->id)}}" class="title-blog">
                                    <h5>{{$blog->title}}</h5>
                                </a>
                                <div class="desc">{!! strip_tags(\Illuminate\Support\Str::limit($blog->content,300)) !!}</div>
                                <a href="{{url('blogs/'.$blog->id)}}" class="link">قراءة المزيد</a>
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>
        </div>
    </section>

    <!-- :: Sponsors -->
    <section class="sponsors-page">
        <div class="container">
            <div class="sec-title">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>عملائنا</h2>
                        <h3>عملاء نفتخر بمشاركتهم النجاح</h3>
                    </div>
                </div>
            </div>
            <div class="sponsors-carousel owl-carousel owl-theme">
                @foreach($customers as $customer)
                    <div class="sponsors-box-item">
                        <img class="img-fluid" src="{{$customer->image}}" alt="01 Sponsors">
                    </div>
                @endforeach


            </div>
        </div>
    </section>

@endsection
