<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HealthCareSection extends Model
{
    protected $table = 'health_care_sections';
    protected $guarded = [];

    protected $appends = ['image'];

    protected $with = 'photo';

    public function photo()
    {

        return $this->morphOne(Photo::class, 'photoable');

    }//end of photo function

    public function getImageAttribute()
    {

        if ($this->photo == '') {
            return asset('default.svg');
        }

        return asset('images/HealthCareSections/' . $this->photo->src);


    }//end of getImageAttribute function
}
