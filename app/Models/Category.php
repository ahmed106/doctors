<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $table = 'categories';
    protected $guarded = [];
    protected $appends = ['image'];

    protected $with = 'photo';

    public function photo()
    {

        return $this->morphOne(Photo::class, 'photoable');

    }//end of photo function

    public function getImageAttribute()
    {

        if ($this->photo == '') {
            return asset('default.svg');
        }

        return asset('images/categories/' . $this->photo->src);


    }//end of getImageAttribute function
}
