<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AboutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();
    }

    public function onStore()
    {
        return [
            'title' => 'required|string',
            'content' => 'required|string',
            'missions' => 'required|string',
            'vision' => 'required|string',
            'plans' => 'required|string',
            'experience_number' => 'required|numeric',
            'healthy_patient' => 'required|numeric',
            'health_departments' => 'required|numeric',
            'doctor_experienced_number' => 'required|numeric',
            'branches' => 'required|numeric',
            'meta_title' => 'sometimes|nullable|string',
            'meta_description' => 'sometimes|nullable|string',
            'meta_keywords' => 'sometimes|nullable|string',
            'photo' => 'sometimes|nullable|file'
        ];

    }//end of onStore function

    public function onUpdate()
    {
        return [
            'title' => 'required|string',
            'content' => 'required|string',
            'missions' => 'required|string',
            'vision' => 'required|string',
            'plans' => 'required|string',
            'experience_number' => 'required|numeric',
            'doctor_experienced_number' => 'required|numeric',
            'healthy_patient' => 'required|numeric',
            'health_departments' => 'required|numeric',
            'branches' => 'required|numeric',
            'meta_title' => 'sometimes|nullable|string',
            'meta_description' => 'sometimes|nullable|string',
            'meta_keywords' => 'sometimes|nullable|string',
            'photo' => 'sometimes|nullable|file'
        ];

    }//end of onUpdate function
}
