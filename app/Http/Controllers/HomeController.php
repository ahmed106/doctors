<?php

namespace App\Http\Controllers;

use App\Helper\UploadTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    use UploadTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {


        $data['blogs_count'] = DB::table('blogs')->count();
        $data['categories_count'] = DB::table('categories')->count();
        $data['customers_count'] = DB::table('customers')->count();


        return view('dashboard.index', compact('data'));
    }

    public function profile()
    {
        return view('dashboard.auth.profile');
    }//end of profile function

    public function editProfile(Request $request)
    {
        $user_id = \auth()->user()->id;
        $author = User::where('id', $user_id)->first();
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email',
            'photo' => 'sometimes|nullable|file'
        ]);
        $data = $request->except('_token', 'photo', 'password');

        if ($request->hasFile('photo')) {
            $this->deleteOldPhoto('images/profile', $author->photo);
            $photo = $this->upload($request->photo, 'profile');

            $data['photo'] = $photo;
        }


        if ($request->password) {
            $data['password'] = Hash::make($request->password);

        } else {
            $data['password'] = \auth()->user()->password;
        }


        $author->update($data);
        return redirect()->back()->with('success', 'تم تعديل البيانات بنجاح');


    }//end of editProfile function
}
