<?php

namespace App\Http\Controllers;

use App\Http\Requests\SliderRequest;
use App\Models\Page;
use App\Models\Slider;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PageController extends Controller
{
    public function index()
    {
        return view('dashboard.pages.index');
    }

    public function data()
    {

        $pages = Page::query()->get();
        $model = 'pages';
        return \Yajra\DataTables\Facades\DataTables::of($pages)
            ->addColumn('actions', function ($raw) use ($model) {
                return view("dashboard.includes.actions", compact('raw', 'model'));
            })
            ->make(true);

    }//end of data function

    public function edit(Page $page)
    {


        return view('dashboard.pages.edit', compact('page'));


    }//end of create function

    public function update(Request $request, Page $page)
    {

        $data = $request->validate([
            'name' => 'required|unique:pages,name,' . $page->id,

        ]);


        $page->update($data);

        return redirect()->route('pages.index')->with('success', 'تم تعديل البيانات بنجاح');

    }//end of update function


}
