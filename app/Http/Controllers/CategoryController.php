<?php

namespace App\Http\Controllers;

use App\Helper\UploadTrait;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CategoryController extends Controller
{
    use UploadTrait;

    public function index()
    {

        return view('dashboard.categories.index');
    }


    public function create()
    {
        return view('dashboard.categories.create');
    }

    public function data()
    {
        $model = 'categories';
        $categories = Category::with('photo')->get();
        return DataTables::of($categories)
            ->addColumn('check_item', function ($raw) {
                return '<input type="checkbox" name="items[]" class="check_item" value="' . $raw->id . '">';
            })
            ->addColumn('photo', function ($raw) {
                return '<img width="100" height="80"  src="' . $raw->image . '" >';
            })
            ->addcolumn('actions', function ($raw) use ($model) {

                return view('dashboard.includes.actions', compact('raw', 'model'));
            })
            ->rawColumns(['photo' => 'photo', 'check_item' => 'check_item'])
            ->make(true);

    }//end of data function


    public function store(CategoryRequest $request)
    {

        $data = $request->validated();
        unset($data['photo']);


        $Service = Category::create($data);

        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'categories');
            $Service->photo()->create([
                'src' => $photo,
                'type' => 'Service',
            ]);
        };

        return redirect()->route('categories.index')->with('success', 'تم إضافه البيانات بنجاح');

    }


    public function edit(Category $category)
    {
        return view('dashboard.categories.edit', compact('category'));
    }

    public function update(CategoryRequest $request, Category $category)
    {
        $data = $request->validated();
        unset($data['photo']);
        $category->update($data);
        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'categories');
            $category->photo ? $this->deleteOldPhoto('images/categories/', $category->photo->src)
                & $category->photo()->update(['src' => $photo]) : $category->photo()->create(['src' => $photo, 'type' => 'categories']);
        }

        return redirect()->route('categories.index')->with('success', 'تم تعديل البيانات بنجاح');


    }


    public function destroy(Category $category)
    {


        $category->photo ? $this->deleteOldPhoto('images/categories/', $category->photo->src) & $category->photo()->delete() : '';
        Category::destroy($category->id);
        return redirect()->route('categories.index')->with('success', 'تم حذف البيانات بنجاح');
    }

    public function bulkDelete(Request $request)
    {
        foreach ($request->items as $item) {
            $service = Category::findOrFail($item);
            $this->destroy($service);
        }
        Category::destroy($request->items);
        return redirect()->route('categories.index')->with('success', 'تم حذف البيانات بنجاح');


    }//end of bulkDelete function
}
