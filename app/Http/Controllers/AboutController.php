<?php

namespace App\Http\Controllers;

use App\Helper\UploadTrait;
use App\Http\Requests\AboutRequest;
use App\Models\About;

class AboutController extends Controller
{
    use UploadTrait;

    public function index()
    {

        $about = About::first();


        return view('dashboard.about.create', compact('about'));
    }


    public function store(AboutRequest $request)
    {
        $data = $request->validated();
        unset($data['photo']);

        $about = About::first();
        if ($about) {
            $about->update($data);
        } else {
            $about = About::create($data);
        }


        if ($request->hasFile('photo')) {

            $about->photo ? $this->deleteOldPhoto('images/about', $about->photo->src) & $about->photo()->delete() : '';
            $photo = $this->upload($request->photo, 'about');
            $about->photo()->create([
                'src' => $photo,
                'type' => 'slider',
            ]);
        };

        return redirect()->route('about.index')->with('success', 'تم إضافه البيانات بنجاح');
    }


}
