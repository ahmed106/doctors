<?php

namespace App\Http\Controllers;

use App\Helper\UploadTrait;
use App\Http\Requests\HealthCareSectionRequest;
use App\Models\HealthCareSection;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class HealthCareSectionController extends Controller
{
    use UploadTrait;

    public function index()
    {

        return view('dashboard.health_care_sections.index');
    }


    public function create()
    {
        return view('dashboard.health_care_sections.create');
    }

    public function data()
    {
        $model = 'health-care';
        $HealthCareSections = HealthCareSection::with('photo')->get();
        return DataTables::of($HealthCareSections)
            ->addColumn('check_item', function ($raw) {
                return '<input type="checkbox" name="items[]" class="check_item" value="' . $raw->id . '">';
            })
            ->addColumn('photo', function ($raw) {
                return '<img width="100" height="80"  src="' . $raw->image . '" >';
            })
            ->addcolumn('actions', function ($raw) use ($model) {

                return view('dashboard.includes.actions', compact('raw', 'model'));
            })
            ->rawColumns(['photo' => 'photo', 'check_item' => 'check_item'])
            ->make(true);

    }//end of data function


    public function store(HealthCareSectionRequest $request)
    {

        $data = $request->validated();
        unset($data['photo']);


        $HealthCareSection = HealthCareSection::create($data);

        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'HealthCareSections');
            $HealthCareSection->photo()->create([
                'src' => $photo,
                'type' => 'HealthCareSection',
            ]);
        };

        return redirect()->route('health-care.index')->with('success', 'تم إضافه البيانات بنجاح');

    }


    public function edit($id)
    {
        $HealthCareSection = HealthCareSection::findOrFail($id);
        return view('dashboard.health_care_sections.edit', compact('HealthCareSection'));
    }

    public function update(HealthCareSectionRequest $request, $id)
    {
        $HealthCareSection = HealthCareSection::findOrFail($id);
        $data = $request->validated();
        unset($data['photo']);
        $HealthCareSection->update($data);
        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'HealthCareSections');
            $HealthCareSection->photo ? $this->deleteOldPhoto('images/HealthCareSections/', $HealthCareSection->photo->src)
                & $HealthCareSection->photo()->update(['src' => $photo]) : $HealthCareSection->photo()->create(['src' => $photo, 'type' => 'HealthCareSection']);
        }

        return redirect()->route('health-care.index')->with('success', 'تم تعديل البيانات بنجاح');


    }


    public function destroy(HealthCareSection $HealthCareSection)
    {
        $HealthCareSection->photo ? $this->deleteOldPhoto('images/HealthCareSections/', $HealthCareSection->photo->src) & $HealthCareSection->photo()->delete() : '';
        HealthCareSection::destroy($HealthCareSection->id);
        return redirect()->route('health-care.index')->with('success', 'تم حذف البيانات بنجاح');
    }

    public function bulkDelete(Request $request)
    {
        foreach ($request->items as $item) {
            $HealthCareSection = HealthCareSection::findOrFail($item);
            $this->destroy($HealthCareSection);
        }
        HealthCareSection::destroy($request->items);
        return redirect()->route('health-care.index')->with('success', 'تم حذف البيانات بنجاح');


    }//end of bulkDelete function
}
