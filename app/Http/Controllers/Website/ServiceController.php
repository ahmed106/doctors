<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Category;

class ServiceController extends Controller
{
    public function index()
    {

        return view('website.pages.services.index');
    }//end of index function

    public function show($id)
    {

        $service = Category::findOrFail($id);
        return view('website.pages.services.show', compact('service'));
    }//end of show function
}
