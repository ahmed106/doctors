<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Customer;
use App\Models\Page;

class BlogController extends Controller
{
    public function index()
    {

        $page_title = Page::whereUrl('/blogs')->first()->name;
        $blogs_ = Blog::with('photo')->simplePaginate(6);
        $customers = Customer::with('photo')->get();
        return view('website.pages.blogs.index', compact('blogs_', 'customers', 'page_title'));

    }//end of index function

    public function show($id)
    {
        $page_title = Page::whereUrl('/blogs')->first()->name;
        $blog = Blog::with('photo')->findOrFail($id);
        $blogs_ = Blog::with('photo')->where('id', '!=', $id)->get();


        return view('website.pages.blogs.show', compact('blog', 'blogs_', 'page_title'));

    }//end of show function
}
