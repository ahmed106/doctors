<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Mail\TestEmail;
use App\Models\ContactUs;
use App\Models\Page;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{

    public function index()
    {

        $page_title = Page::whereUrl('/contact-us')->first()->name;

        return view('website.pages.contact_us.index', compact('page_title'));

    }//end of index function

    public function post(Request $request)
    {

        $data = $request->validate([
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'body' => 'required|string'
        ]);


        ContactUs::create($data);
        $setting_email = Setting::first()->website_email;
        Mail::to($setting_email)->send(new TestEmail($data));
        return redirect()->back()->with('success', 'تم إرسال طلبك بنجاح');

    }//end of post function
}
