<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Customer;
use App\Models\HealthCareSection;
use App\Models\Service;
use App\Models\Slider;

class FrontController extends Controller
{
    public function index()
    {


        $sliders = Slider::with('photo')->orderBy('id', 'desc')->get();
        $categories = Category::with('photo')->get();
        $customers = Customer::with('photo')->get();
        $blogs = Blog::with('photo')->get();
        $about = About::with('photo')->first();
        $services = Service::with('photo')->get()->take(3);
        $health_care = HealthCareSection::get();
        return view('home', compact('sliders', 'categories', 'services', 'customers', 'blogs', 'about', 'health_care'));

    }//end of index function
}
