<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Page;

class CategoryController extends Controller
{
    public function index()
    {

        $page_title = Page::whereUrl('/categories')->first()->name;

        $categories = Category::with('photo')->get();


        return view('website.pages.categories.index', compact('categories', 'page_title'));

    }//end of index function

    public function show($id)
    {
        $page_title = Page::whereUrl('/categories')->first()->name;
        $category = Category::with('photo')->findOrFail($id);
        $categories = Category::with('photo')->get();
        return view('website.pages.categories.show', compact('category', 'categories', 'page_title'));

    }//end of show function
}
