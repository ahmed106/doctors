<?php

namespace App\Providers;

use App\Models\About;
use App\Models\Blog;
use App\Models\Page;
use App\Models\Setting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('services') || Schema::hasTable('settings')) {

            $setting = Setting::first();
            $about = About::with('photo')->first();
            $blogs = Blog::with('photo')->get();
            $pages = Page::get();
            View::composer('*', function ($view) use ($setting, $blogs, $about, $pages) {

                $view->with(['setting' => $setting]);
                $view->with(['about' => $about]);
                $view->with(['pages' => $pages]);
                $view->with(['blogs' => $blogs]);
            });

        }
        Schema::defaultStringLength(191);
    }
}
